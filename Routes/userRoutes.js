var express = require('express');
const router = express.Router()

var bcrypt = require('bcrypt');
const saltRounds = 10;


const User = require('../Model/userModel')


router.get("/", async (req, res) => {
    const users = await User.find()
    console.log(users)
    res.json({
        users: users
    })
})

router.post("/", async (req, res) => {
    const username = req.body.username
    const password = req.body.password
    const salt = await bcrypt.genSalt(saltRounds)
    const hashedPassword = await bcrypt.hash(password, salt)

    const newUser = new User({
        username: username,
        password: hashedPassword
    })
    newUser.save().then((newUser) => {
        res.status(201).send(newUser)
    }).catch((error) => {
        res.status(400).send(error);
    })
})

router.get("/user-check", async (req, res) => {
    const username = req.body.username
    const password = req.body.password


    const checkPassword = async (username, password) => {
        const salt = await bcrypt.genSalt(saltRounds)
        const hashedPassword = bcrypt.hash(password, salt)
        const userInfo = User.find({ username: username })
        if (userInfo.password === hashedPassword) {
            return true
        }
        else if (userInfo.password != hashedPassword) {
            return false
        }
    }

    console.log(checkPassword)
    return checkPassword
})

module.exports = router