var express     = require('express');
const router = express.Router()

const Booking=require("../Model/bookingModel")

router.post("/", async (req, res) => {
    const booked_by = req.body.booked_by
    const phone = req.body.phone
    const show_id = req.body.show_id
    console.log(`received ${req.body.booked_by}`)
    const newBooking = new Booking({
        booked_by: booked_by,
        phone: phone,
        show_id:show_id
    })
    newBooking.save().then((newBooking) => {
        res.status(201).send(newBooking);
    }).catch((error) => {
        res.status(400).send(error);
    })
})


router.get("/", async (req, res) => {
    const bookings = await Booking.find()
    res.json({
        bookings: bookings
    })
})


router.get("/:booking_id", async (req, res) => {
    const booking_id=req.params.booking_id
    console.log(booking_id,"code runs")
    const bookings = await Booking.find({
        _id:booking_id
    })
    res.json(bookings)
})

router.get("/phone/:phone", async (req, res) => {
    const phone=req.params.phone
    const bookings = await Booking.find({phone:phone})
    res.json(bookings)
})

module.exports = router