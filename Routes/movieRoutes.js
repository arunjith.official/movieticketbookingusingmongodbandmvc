var express     = require('express');
const router = express.Router()

const Movie=require("../Model/movieModel")


router.get("/", async (req, res) => {
    const movie = await Movie.find()
    console.log(movie)
    res.json({
        movies: movie
    })
})



router.get("api/:movie_id", async (req, res) => {
    const movie_id=req.params.movie_id
    const movies = await Movie.find({
        _id:movie_id
    })
    res.json(movies)
})
 
router.post("/", async (req, res) => {
    const movie_name = req.body.movie_name
    const movie_duration = req.body.movie_duration
    const release_date = req.body.release_date
    const movie_description = req.body.movie_description

    console.log(req.body)
    const newMovie = new Movie({
        movie_name:movie_name,
        movie_duration:movie_duration,
        release_date:release_date,
        movie_description:movie_description
    })
    newMovie.save().then((newMovie)=> {
        res.status(201).send(newMovie);
    }).catch((error) => {
        res.status(400).send(error);
    })
});



module.exports = router