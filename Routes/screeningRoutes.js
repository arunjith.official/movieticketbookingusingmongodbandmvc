var express = require('express');
const router = express.Router()


const Screening = require('../Model/screeningModel');


router.get("/show/:show_id", async (req, res) => {
    const show_id = req.params.show_id
    const screening = await Screening.find({
        _id: show_id
    }).populate(['movie_id', 'screen_id'])
    res.json({
        screening: screening
    })
})

router.get("/", async (req, res) => {
    const screening = await Screening.find()
    res.json({
        screening: screening
    })
})



router.get("/movie/:movie_id", async (req, res) => {
    const movie_id = req.params.movie_id
    const screening = await Screening.find({
        movie_id: movie_id
    }).populate(['movie_id','screen_id'])
    res.json({
        screening:screening
    })
})


router.post("/", async (req, res) => {
    const movie_id = req.body.movie_id
    const screen_id = req.body.screen_id
    const show_time = req.body.show_time

    const newScreening = new Screening({
        movie_id: movie_id,
        screen_id: screen_id,
        show_time, show_time
    })
    newScreening.save().then((newScreening) => {
        res.status(201).send(newScreening);
    }).catch((error) => {
        res.status(400).send(error)
    })

})




module.exports = router