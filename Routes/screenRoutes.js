var express = require('express');
const router = express.Router()




const Screen = require('../Model/screenModel')


router.get("/", async (req, res) => {
    const screen = await Screen.find()
    console.log(screen)
    res.json({
        screen: screen
    })

})


router.post('/', async (req, res) => {
    const total_seats = req.body.total_seats
    const screen_name = req.body.screen_name
    const screen_description = req.body.screen_description


    const newScreen = new Screen({
        total_seats:total_seats,
        screen_name:screen_name,
        screen_description:screen_description
    })
    newScreen.save().then((newScreen) => {
        res.status(201).send(newScreen);
    }).catch((error) => {
        res.status(400).send(error);
    })
})

router.get("/:screen_id", async (req, res) => {
    const screen_id = req.params.screen_id
    const screen = Screen.find({ _id: screen_id })
    res.json(screen)
})

module.exports = router