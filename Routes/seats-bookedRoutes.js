var express = require('express');
const router = express.Router()




const SeatsBooked = require('../Model/seatsBookedModel')

//seats-booked
router.post("/", async (req, res) => {
    const seat_id = req.body.seat_id
    const booking_id = req.body.booking_id
    const show_id = req.body.show_id

    const bookSeat = new SeatsBooked({
        seat_id: seat_id,
        booking_id: booking_id,
        show_id: show_id
    })
    bookSeat.save().then((bookSeat) => {
        res.status(201).send(bookSeat)
    }).catch((error) => {
        res.status(400).send(error);
    })
})

router.get("/", async (req, res) => {
    const seats_booked = await SeatsBooked.find()
    console.log(seats_booked)
    res.json({
        seats_booked: seats_booked
    })
})

router.get("/:show_id", async (req, res) => {
    const show_id = req.params.show_id
    try {
        const seatsBooked = await SeatsBooked.find({
            show_id: show_id
        })
        res.json({
            seats_booked: seatsBooked
        })
    }
    catch (err) {
        res.json({
            seats_booked: []
        })
    }
})

router.get("/booking/:booking_id", async (req, res) => {
    console.log("it runs")
    const booking_id = req.params.booking_id
    console.log(booking_id)
    const seatsBooked = await SeatsBooked.find({
        booking_id: booking_id
    })
    res.json(seatsBooked)
})

module.exports = router