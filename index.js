require('dotenv').config()

const express = require('express')
const mongoose = require('mongoose')
var cors = require('cors')



const userRouter = require("./Routes/userRoutes")
const seatsRouter = require("./Routes/seats-bookedRoutes")
const screenRouter = require("./Routes/screenRoutes")
const screeningRouter = require("./Routes/screeningRoutes")
const movieRouter = require("./Routes/movieRoutes")
const bookingRouter = require("./Routes/bookingRoutes")

const app = express()
const port = 3000

const url = process.env.DBURL

// localhost

// mongoose.connect(url, {useNewUrlPareser:true})
// const con = mongoose.connection

// con.on('open',function(){
//   console.log("connected....")
// })

// online

main().then( function() {
  console.log("connected.....")
}).catch(err => console.log(err));

async function main() {
  await mongoose.connect(url);
}

app.use(cors())
app.use(express.json())


app.use("api/user", userRouter)
app.use("api/seats-booked", seatsRouter)
app.use("api/screen", screenRouter)
app.use("api/screening", screeningRouter)
app.use("api/movie",movieRouter)
app.use("api/book", bookingRouter)
app.get('/', (req, res) => {
  res.send('Hello World!')
})


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})