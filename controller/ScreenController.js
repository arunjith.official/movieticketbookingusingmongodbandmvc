const Screen = require("../Model/schemas/ScreenSchema")

const getScreen = async () => {
    const seats = await Screen.findAll({
        attributes: ['screen_id', 'total_seats', 'screen_name', 'screen_description']
    });
    const seatsArray = seats.map(seats => seats.dataValues)
    return seatsArray
}

const createNewScreen = async (screen_id, total_seats, screen_name, screen_description) => {
    const newscreen = await Screen.create({
        screen_id,
        total_seats,
        screen_name,
        screen_description
    })
}

exports.getScreen = getScreen;
exports.createNewScreen = createNewScreen;