const Booking = require("../Model/schemas/BookingSchema")


const getBooking = async  () => {
    const booking = await Booking.findAll({
        attributes : ["booking_id","booked_by","phone","show_id"]
    })
    const bookingArray = booking.map(booking => booking.dataValues)
    return bookingArray
}

const createBooking = async (booked_by, phone, show_id) => {
    const booking = await Booking.create({
        booked_by,
        phone,
        show_id
    })
    return booking
}

const getBookingsByPhone = async (phone) => {
    const booking = await Booking.findAll({
        where: { phone: phone}
    })
    console.log(booking)
    return booking
}

const getbookingsByBookingID = async (bookingID) => {
    const booking = await Booking.findAll({
        where:{ booking_id:bookingID }
    })
    console.log(booking)
    return booking
}

exports.getbookingsByBookingID = getbookingsByBookingID;
exports.getBookingsByPhone = getBookingsByPhone;
exports.createBooking = createBooking;
exports.getBooking = getBooking;