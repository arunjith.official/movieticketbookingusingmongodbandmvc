

const SeatsBooked = require("../Model/schemas/Seats-bookedSchema")

const createSeatsBooked = async (seat_id, booking_id, show_id) => {
    const seatsbooked = await SeatsBooked.create({
        seat_id,
        booking_id,
        show_id
    })
}

exports.createSeatsBooked = createSeatsBooked


const getSeats = async () => {
    const seats = await SeatsBooked.findAll({
        attributes: ['booking_id', 'seat_id', 'show_id']
    });
    const seatsArray = seats.map(seats => seats.dataValues)
    return seatsArray
}

exports.getSeats = getSeats;

const getSeatsBooked = async () => {
    const seatsbooked = await SeatsBooked.findAll({
        attributes: ['seat_id', 'booking_id', 'show_id']
    })
    const seatsBookedArray = seatsbooked.map(SeatsBooked => SeatsBooked.dataValues)
    return seatsBookedArray
}

exports.getSeatsBooked = getSeatsBooked

const getSeatsBookedByShowID = async (show_id) => {
    const seats = await SeatsBooked.findAll({
        where: { show_id: show_id }
    })
    console.log(seats)
    return seats
}

exports.getSeatsBookedByShowID = getSeatsBookedByShowID

const getSeatsBookedByBookingID = async (booking_id) => {
    const seats = await SeatsBooked.findAll({
        where: { booking_id: booking_id }
    })
    console.log(seats)
    return seats
}

exports.getSeatsBookedByBookingID=getSeatsBookedByBookingID