const { ObjectID } = require('bson');
const mongoose = require('mongoose')
const { Schema } = mongoose;

const seatsBookedSchema = new Schema({
    seat_id: Number,
    booking_id:{type:ObjectID,ref:"booking"},
    show_id: {type:ObjectID,ref:"screening"}
})

module.exports=mongoose.model("seatsBooked",seatsBookedSchema)