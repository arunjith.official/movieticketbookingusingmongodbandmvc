const mongoose = require('mongoose')
const { Schema } = mongoose;

const screenSchema = new Schema({
    total_seats: Number,
    screen_name: String,
    screen_description: String,
})

module.exports=mongoose.model("screen",screenSchema)