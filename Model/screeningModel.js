const { ObjectID } = require('bson');
const mongoose = require('mongoose')
const { Schema } = mongoose;

const screeningSchema = new Schema({
    show_time: Date,
    movie_id: {type:ObjectID,ref:"movie"},
    screen_id: {type:ObjectID,ref:"screen"}
})

module.exports=mongoose.model("screening",screeningSchema)