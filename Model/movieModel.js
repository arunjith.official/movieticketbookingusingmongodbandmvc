const mongoose = require('mongoose')
const { Schema } = mongoose;

const movieSchema = new Schema({
    movie_name: String,
    movie_duration: Number,
    release_date: {type: Date, default:Date.now},
    movie_description: String
})

module.exports=mongoose.model("movie",movieSchema)