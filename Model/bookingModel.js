const { ObjectId } = require('mongodb');
const mongoose= require('mongoose')
const { Schema } = mongoose;

const bookingSchema = new Schema({
    booked_by: String,
    phone: Number,
    show_id: {type:ObjectId,ref:"screening"}
})

module.exports=mongoose.model("booking",bookingSchema)